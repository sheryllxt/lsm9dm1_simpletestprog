#include "mbed.h"
#include "SerialStream.h"
#include "LSM9DS1.h"

//Shift left 1bit I2C address
uint8_t AGadd = 0x6B <<1;
uint8_t Madd  = 0x1E <<1;


//Serial Communication
BufferedSerial serial(PG_8, PG_7);
SerialStream<BufferedSerial>pc(serial);


//Testing if board is working
DigitalOut led(LED1);
DigitalOut TOF(PF_5);

//Variables 
  float accelX , accelY , accelZ;
  float gyroX, gyroY, gyroZ;
  float magX, magY, magZ;
  float temp;  

int main()
    {

    //TOF power on    
    TOF = 1;

    LSM9DS1 IMU(PF_0, PF_1, AGadd, Madd);

    uint16_t ret = IMU.begin();

     if (!ret) {
        pc.printf("Failed to communicate with LSM9DS1...\n");
    }

    IMU.calibrate(true);     
    IMU.calibrateMag(true);

    while (true) {

        IMU.readTemp();
        IMU.readAccel();
        IMU.readGyro();
        IMU.readMag();


        //Accelerometer in gauss 
        accelX = IMU.calcAccel(IMU.ax);
        accelY = IMU.calcAccel(IMU.ay);
        accelZ = IMU.calcAccel(IMU.az);

       //Gyroscope in gps
        gyroX = IMU.calcGyro(IMU.gx);
        gyroY = IMU.calcGyro(IMU.gy);
        gyroZ = IMU.calcGyro(IMU.gz);

        //Magnetomter in Gauss

        magX = IMU.calcMag(IMU.mx);
        magY = IMU.calcMag(IMU.my);
        magZ = IMU.calcMag(IMU.mz);

        //Temperature 
        temp = IMU.TempCelsius(IMU.temperature);

        pc.printf("\nIMU Temperature = %f C\n\r",temp);
        pc.printf("accel: X: %.5f  , Y= %.5f , Z= %.5f in gs\r\n", accelX, accelY, accelZ);
        pc.printf("gyro: X: %.5f  , Y= %.5f , Z= %.5f in dps \r\n", gyroX, gyroY, gyroZ);
        pc.printf("mag: X: %.5f  , Y= %.5f , Z= %.5f in gauss\r\n", magX, magY, magZ);
    
        led =! led;
        ThisThread::sleep_for(500);
    }
} 