/******************************************************************************
SFE_LSM9DS1.h
SFE_LSM9DS1 Library Header File
Jim Lindblom @ SparkFun Electronics
Original Creation Date: February 27, 2015
https://github.com/sparkfun/LSM9DS1_Breakout

This file prototypes the LSM9DS1 class, implemented in SFE_LSM9DS1.cpp. In
addition, it defines every register in the LSM9DS1 (both the Gyro and Accel/
Magnetometer registers).

Development environment specifics:
    IDE: Arduino 1.6.0
    Hardware Platform: Arduino Uno
    LSM9DS1 Breakout Version: 1.0

This code is beerware; if you see me (or any other SparkFun employee) at the
local, and you've found our code helpful, please buy us a round!

Distributed as-is; no warranty is given.
******************************************************************************/

/* Modified by:
 * Lee XinTing Sheryl    <sheryllxt@gmail.com>
 */

#ifndef LSM9DS1_H
#define LSM9DS1_H


/* Includes ------------------------------------------------------------------*/
#include "mbed.h"
#include <stdint.h>
#include "LSM9DS1_Registers.h"
#include "LSM9DS1_Types.h"


/* Sensor Individual Axis */
enum lsm9ds1_axis {
    X_AXIS, 
    Y_AXIS, 
    Z_AXIS, 
    ALL_AXIS   
};


class LSM9DS1
{
public:
    
    /** LSM9DS1 class constructor 
     * @param sda       SDA pin
     * @param io0       SCL pin
     * @param xgAddr    I2C address of the Accelemeter/Gyroscope shifted by 1 bit
     * @param mgAddr    I2C address of the Magnetometer shifted by 1 bit   
     */
    LSM9DS1(PinName sda, PinName scl, uint8_t xgAddr, uint8_t mAddr);
       
    
    /** Initialization set up 
     *
     * Initialize the Gyroscope, Accelerometer, and Magnetometer.
     * Set up scale and output rate of each sensor
     * Values set in IMUSetting struct will take effect after calling this function
     */
    uint16_t begin();
    
    /** Calibration for Gyroscope, Accelerometer 
     *
     *@param autoCalc    setting function to be always true
     */
    void calibrate(bool autoCalc = true);

    /** Calibration for Magnetometer 
     *
     *@param loadIn     setting function to be always true
     */
    void calibrateMag(bool loadIn = true);

    /** Magnetometer Offset  
     *
     * This function calculate the Magnetomter offset used to compensate environmental effects
     * Calculated value will be write to respective OFFSET register 
     * This value acts on the magnetic output data value
     *
     *@param axis          axis obtain from calibrateMag
     *@param offset        offset obtain from calibrateMag
     */
    void magOffset(uint8_t axis, int16_t offset);
    
    /** accelAvailable() -- Polls the accelerometer status register to check
    * if new data is available.
    * Output:  1 - New data available
    *          0 - No new data available
    */

    /** Poll Accelerometer status register
     *
     *  Check if new data is available
     *
     *  @return         1 - New data available
     *                  0 - No new data available 
     */
    uint8_t accelAvailable();
    
    /** Poll Gyroscope status register
     *
     *  Check if new data is available
     *
     *  @return         1 - New data available
     *                  0 - No new data available 
     */
    uint8_t gyroAvailable();
    
    /** Poll Temperature status register
     *
     *  Check if new data is available
     *
     *  @return         1 - New data available
     *                  0 - No new data available 
     */
    uint8_t tempAvailable();
    
     /** Poll Magnetometer status register
     *
     *  Check if new data is available on specific axis or all axis (default)
     *
     *  @param  axis    axis can be either X_AXIS, Y_AXIS, Z_AXIS, ALL_AXIS from lsm9ds1_axis enum  
     *                  
     *  @return         1 - New data available
     *                  0 - No new data available 
     */
    uint8_t magAvailable(lsm9ds1_axis axis = ALL_AXIS);
    
    /** readGyro() -- Read the gyroscope output registers.
    * This function will read all six gyroscope output registers.
    * The readings are stored in the class' gx, gy, and gz variables. Read
    * those _after_ calling readGyro().
    */
    
     /** Read Gyroscope output register
     *
     *  This fuction will read all six Gyroscope output registers
     *  Reading are stored in the class' gx, gy and gz variables
     *  Read those only after calling readGyro()
     *
     */
    void readGyro();
    
     /** Read specific axis from Gyroscope
     *
     *  Axis can be any of X_AXIS, Y_AXIS, or Z_AXIS from lsm9ds1 enum
     *  
     *  @param axis     can be either X_AXIS, Y_AXIS, or Z_AXIS
     *  @return         16-bit signed integer with sensor data on requested axis 
     */
    int16_t readGyro(lsm9ds1_axis axis);
    
     /** Read Accelerometer output register
     *
     *  This fuction will read all six Accelerometer output registers
     *  Reading are stored in the class' ax, ay and az variables
     *  Read those only after calling readAccel()
     *
     */
    void readAccel();
    
    /** Read specific axis Accelerometer
     *
     *  Axis can be any of X_AXIS, Y_AXIS, or Z_AXIS from lsm9ds1 enum
     *  
     *  @param axis     can be either X_AXIS, Y_AXIS, or Z_AXIS
     *  @return         16-bit signed integer with sensor data on requested axis 
     */
    int16_t readAccel(lsm9ds1_axis axis);
    
     /** Read Magnetometer output register
     *
     *  This fuction will read all six Magnetomter output registers
     *  Reading are stored in the class' ax, ay and az variables
     *  Read those only after calling readAccel()
     *
     */
    void readMag();
    
    /** Read specific axis from Magnetometer
     *
     *  Axis can be any of X_AXIS, Y_AXIS, or Z_AXIS from lsm9ds1 enum
     *  
     *  @param axis     can be either X_AXIS, Y_AXIS, or Z_AXIS
     *  @return         16-bit signed integer with sensor data on requested axis 
     */
    int16_t readMag(lsm9ds1_axis axis);

     /** Read Temperature output register
     *
     *  This fuction will read two temperature output registers
     *  The combined reading are stored in class' temperature variables
     *  Read those only after calling readTemp()
     *
     */
    void readTemp();
    
    /** Convert RAW signed 16-bit Gyroscope value to degrees per second
     *
     *  This function read in a signed 16-bit value and returns the scaled DPS
     *  This fuction relies on correct gScale and gRes value
     *  
     *  @param gyro     A signed 16-bit raw reading from Gyroscope
     *  @return         Scaled DPS value 
     */
    float calcGyro(int16_t gyro);

     /** Convert RAW signed 16-bit Accelerometer value to gravity (g's)
     *
     *  This function read in a signed 16-bit value and returns the scaled g's.
     *  This fuction relies on correct aScale and aRes value
     *  
     *  @param accel    A signed 16-bit raw reading from Accelerometer
     *  @return         Scaled g's value 
     */
    float calcAccel(int16_t accel);

     /** Convert RAW signed 16-bit Magnetometer value to Gauss (Gs)
     *
     *  This function read in a signed 16-bit value and returns the scaled Gs.
     *  This fuction relies on correct mScale and mRes value
     *  
     *  @param mag      A signed 16-bit raw reading from Magnetometer
     *  @return         Scaled Gs value 
     */
    float calcMag(int16_t mag);
    
     /** Set the full-scale range of the Gyroscope
     *
     *  This function can be called to set the scale of the Gyroscope 
     *  Scale range include 245, 500, or 200 degrees per second
     *  
     *  @param gScl     Desired possible Gyroscope scale values 
     *                  from gyro_scale enum 
     */
    void setGyroScale(uint16_t gScl);
    
     /** Set the full-scale range of the Accelerometer
     *
     *  This function can be called to set the scale of the Accelerometer
     *  Scale range include 2, 4, 6, 8, or 16 g's 
     *  
     *  @param aScl     Desired possible Accelerometer scale values 
     *                  from accel_scale enum 
     */
    void setAccelScale(uint8_t aScl);
    
     /** Set the full-scale range of the Magnetometer
     *
     *  This function can be called to set the scale of the Magnetometer 
     *  Scale range include 2, 4, 8, or 12 Gs
     *  
     *  @param mScl     Desired possible Magnetometer scale values 
     *                  from mag_scale enum 
     */
    void setMagScale(uint8_t mScl);
    
     /** Set the output data rate and bandwidth of the Gyroscope
     *
     *  This function can be called to set ODR of the Gyroscope
     *  All possible data rate/bandwidth combos includes:
     *  Power down, 14.9, 59.5, 119, 238, 476 or 952 Hz
     *  
     *  @param gRate    Desired possible ODR from  
     *                  from gyro_odr enum 
     */
    void setGyroODR(uint8_t gRate);
    
     /** Set the output data rate of the Accelerometer
     *
     *  This function can be called to set ODR of the Accelerometer
     *  All possible data rate/bandwidth combos includes:
     *  Power down, 10, 50, 119, 238, 476 or 952 Hz
     *  
     *  @param aRate    Desired possible ODR from  
     *                  from accel_odr enum 
     */
    void setAccelODR(uint8_t aRate);    

     /** Set the output data rate of the Magnetometer
     *
     *  This function can be called to set ODR of the Magnetometer
     *  All possible data rate/bandwidth combos includes:
     *  0.625, 1.25, 2.5, 5, 10, 20, 40 or 80 Hz
     *  
     *  @param mRate    Desired possible ODR from  
     *                  from mag_odr enum 
     */
    void setMagODR(uint8_t mRate);

    /** Configure Inactivity Interrupt parameters
     *
     *@param duration      Inactivity duration, actual value depend on Gyroscope ODR
     *@param threshold     Activity Threshold
     *@param sleepOn       True: Gyroscope in sleepmode
     *                     False: Gyroscope in power-down
     */
    void configInactivity(uint8_t duration, uint8_t threshold, bool sleepOn);

     /** Configure Accelerometer Interrupt Generator 
     *
     *@param generator     Interrupt axis/high-low events
     *                     Any OR'd combination of ZHIE_XL, ZLIE_XL, YHIE_XL, YLIE_XL, XHIE_XL, XLIE_XL       
     *@param andInterrupts AND/OR combination of interrupt events
     *                     True: AND combination
     *                     False: OR combination
     */
    void configAccelInt(uint8_t generator, bool andInterrupts = false);
    
     /** Configure the threshold of an Accelerometer axis
     *
     *@param threshold     Interrupt threshold, Possible values: 0-255
     *                     Multiply by 128 to get the actual raw accel value
     *@param [in]          axis is the desired axis to be configured, either X_AXIS, Y_AXIS, or Z_AXIS
     *@param duration      Duration value, it must be above or below threshold of trigger interrupt           
     *@param wait          Wait function on duration counter
     *                     True: Wait for duration samples before exiting interrupt
     *                     False: Wait function off                           
     */
    void configAccelThs(uint8_t threshold, lsm9ds1_axis axis, uint8_t duration = 0, bool wait = 0);
  
     /** Configure Gyroscope Interrupt Generator
     *
     *@param generator     Interrupt axis/high-low events
     *                     Any OR'd combination of ZHIE_G, ZLIE_G, YHIE_G, YLIE_G, XHIE_G, XLIE_G
     *@param aoi           AND/OR combination of interrupt events
     *                     True: AND combination
     *                     False: OR combination
     *@param latch         Latch Gyroscope Interrupt Request                         
     */
    void configGyroInt(uint8_t generator, bool aoi, bool latch);
    
     /** Configure the threshold of a Gyroscope axis
     *
     *@param threshold     Interrupt threshold, Possible value: 0-0x7FF
     *                     Value is equivalent to raw gyroscope value
     *@param [in]          axis is the desired axis to be configured, either X_AXIS, Y_AXIS, or Z_AXIS
     *@param duration      Duration value, it must be above or below threshold of trigger interrupt
     *@param wait          Wait function on duration counter
     *                     True: Wait for duration samples before exiting interrupt
     *                     False: Wait function off                
     */
    void configGyroThs(int16_t threshold, lsm9ds1_axis axis, uint8_t duration, bool wait);
    
     /** Configure INT1 or INT2 (For Gyroscope and Accelerometer Interrupts only)
     *
     *@param interrupt     Select INT1 or INT2, Possible values: XG_INT1 or XG_INT2
     *@param generator     Or'd combination of interrupt generators
     *                     Possible values: INT_DRDY_XL, INT_DRDY_G, INT1_BOOT (INT1 only), INT2_DRDY_TEMP (INT2 only)
     *@param activeLow     Interrupt active configuration, Either INT_ACTIVE_HIGH or INT_ACTIVE_LOW
     *@param pushPull      Push-pull or open drain interrupt configuration, Either INT_PUSH_PULL or INT_OPEN_DRAIN    
     */
    void configInt(interrupt_select interupt, uint8_t generator,
                   h_lactive activeLow = INT_ACTIVE_LOW, pp_od pushPull = INT_PUSH_PULL);
                   
     /** Configure Magnetometer Interrupt Generator
     *
     *@param generator     Interrupt axis/high-low events, 
     *                     Any OR'd combination of ZIEN, YIEN, XIEN
     *@param [in]          activeLow is the Interrupt active configuration,
     *                     Either INT_ACTIVE_HIGH or INT_ACTIVE_LOW
     *@param latch         Gyroscope Interrupt request    
     */
    void configMagInt(uint8_t generator, h_lactive activeLow, bool latch = true);
    
     /** Configure the threshold of a Magnetometer axis
     *
     *@param threshold     Interrupt threshold, Possible values: 0-0x7FF
     *                     Value is equivalent to raw magnetometer value.
     */
    void configMagThs(uint16_t threshold);
    
     /** Get contents of Gyroscope Interrupt Source Register
     */
    uint8_t getGyroIntSrc();
    
     /** Get contents of Accelerometer Interrupt Source Register
     */
    uint8_t getAccelIntSrc();
    
     /** Get contents of Magnetometer Interrupt Source Register
     */
    uint8_t getMagIntSrc();
    
     /** Get status of Inactivity Interrupt
     */
    uint8_t getInactivity();
    
    /** Sleep or wake the Gyroscope
     *
     *@param enable        True: Sleep Gyroscope 
     *                     False: Wake Gyroscope
     */
    void sleepGyro(bool enable = true);

    /** Enable or disable FIFO
     *
     *@param enable        True: Enable 
     *                     False: Disable
     */
    void enableFIFO(bool enable = true);

    /** Configure FIFO mode and Threshold
     *
     *@param fifoMode:     Set FIFO mode to off, FIFO (stop when full), continuous, bypass
     *                     Possible inputs: FIFO_OFF, FIFO_THS, FIFO_CONT_TRIGGER, FIFO_OFF_TRIGGER, FIFO_CONT
     *@param fifoThs       FIFO threshold level setting
     *                     Any value from 0-0x1F is acceptable.
     */
    void setFIFO(fifoMode_type fifoMode, uint8_t fifoThs);
    
    /** Get number of FIFO samples
     */
    uint8_t getFIFOSamples();

    /////////////////////
    // Added Function //
    ////////////////////

    /** To check and verify communication 
     */   
    uint8_t GetI2CStatus();  

    /** Convert chip temperature to Celsius
     */ 
    float TempCelsius(int16_t tempval);

    /* IMU device setting struct from LSM9DS1_Types.h */
    IMUSettings settings;

    /* I2C return status  */
    enum Error{

        HAVEACK,
        NOACK,   
    };
    
    /** Store gyroscope, acceleometer and magnetometer reading in a series of public class variables
     *
     * Each sensor gets three variables -- one for each axis 
     * Call readGyro(), readAccel() and readMag() first, before using these variables 
     *
     */
    int16_t gx, gy, gz; /* x, y, and z axis 16-bit readings of the gyroscope */
    int16_t ax, ay, az; /* x, y, and z axis 16-bit readings of the accelerometer */
    int16_t mx, my, mz; /* x, y, and z axis 16-bit readings of the magnetometer */
    int16_t temperature; /* Chip temperature */
    float degcelsius;   /* Temperature in degree celsius */
    float gBias[3], aBias[3], mBias[3]; /*Bias variables used for calibration */
    int16_t gBiasRaw[3], aBiasRaw[3], mBiasRaw[3]; /*Bias variables used for calibration */
    

protected:  
    
     /** Sets up the Gyroscope, Acceleration and Magnetometer to default setting
     *
     *@param interface     Set interface mode: IMU_MODE_I2C
     *@param xgAddr        Set the I2C address of the Acceleromter/Gyroscope
     *@param mAddr         Set the I2C address of the Magnetometer               
     */
    void init(interface_mode interface, uint8_t xgAddr, uint8_t mAddr);

    /** Sets up the Gyroscope to begin reading
     *
     * This function steps through all the five Gyroscope control registers
     * Following parameters will be set upon exiting:
     *
     * CTRL_REG1_G = 0x0F: Normal operation mode, all axes enabled, 95 Hz ODR, 12.5 Hz cutoff frequency.
     * CTRL_REG2_G = 0x00: HPF set to normal mode, cutoff frequency set to 7.2 Hz (depends on ODR).
     * CTRL_REG3_G = 0x88: Interrupt enabled on INT_G (set to push-pull and active high). 
     *                     Data-ready output enabled on DRDY_G.
     * CTRL_REG4_G = 0x00: Continuous update mode. Data LSB stored in lower address. 
     *                     Scale set to 245 DPS. SPI mode set to 4-wire.
     * CTRL_REG5_G = 0x00: FIFO disabled. HPF disabled.
     */
    void initGyro();
    
     /** Sets up the Accelerometer to begin reading
     *
     * This function steps through all the Accelerometer related control registers
     * Following parameters will be set upon exiting:
     *
     * CTRL_REG0_XM = 0x00: FIFO disabled, HPF bypassed and Normal mode
     * CTRL_REG1_XM = 0x57: 100 Hz data rate, Continuous update and all Axis enabled
     * CTRL_REG2_XM = 0x00: 2g scale, 773 Hz anti-alias filter BW
     * CTRL_REG3_XM = 0x04: Accel data ready signal on INT1_XM pin.
     */
    void initAccel();
    
     /** Sets up the Magnetometer to begin reading
     *
     * This function steps through all the Magnetometer-related control registers
     * Following parameters will be set upon exiting:
     *
     * CTRL_REG4_XM = 0x04: Magnetometer data ready signal on INT2_XM pin.
     * CTRL_REG5_XM = 0x14: 100 Hz update rate. Low resolution. Interrupt requests don't latch. 
     *                      Temperature sensor disabled.
     * CTRL_REG6_XM = 0x00: 2 Gs scale.
     * CTRL_REG7_XM = 0x00: Continuous conversion mode, Normal HPF mode.
     * INT_CTRL_REG_M = 0x09: Interrupt active-high, Enable interrupts.
     */
    void initMag();
    
    /** Reads a byte from a specified Magnetometer Register
     *
     *@param subAddress    Register to be read from
     *@return              An 8-bit value read from the requested address               
     */
    uint8_t mReadByte(uint8_t subAddress);
    
    /** Reads a number of bytes, beginning at an address and incrementing from the Magnetometer
     *
     *@param subAddress    Register to be read from
     *@param[in]           dest is a pointer to an array to store the values on return
     *@param count         The number of bytes to be read              
     */
    void mReadBytes(uint8_t subAddress, uint8_t * dest, uint8_t count);
    
    /** Write a byte to a register in the Magnetometer
     *
     *@param subAddress    Register to be written to
     *@param data          Data to be written to the register            
     */
    void mWriteByte(uint8_t subAddress, uint8_t data);
    
    /** Reads a byte from a register in the Gyroscope/Accelerometer Register
     *
     *@param subAddress    Register to be read from
     *@return              An 8-bit value read from the requested address               
     */
    uint8_t xgReadByte(uint8_t subAddress);

    /** Reads a number of bytes, beginning at an address and incrementing from the Gyroscope/Accelerometer
     *
     *@param subAddress    Register to be read from
     *@param[in]           dest is a pointer to an array to store the values on return
     *@param count         The number of bytes to be read              
     */
    void xgReadBytes(uint8_t subAddress, uint8_t * dest, uint8_t count);
    
     /** Write a byte to a register in the Gyroscope/Accelerometer
     *
     *@param subAddress    Register to be written to
     *@param data          Data to be written to the register            
     */
    void xgWriteByte(uint8_t subAddress, uint8_t data);

    /** Calculate the resolution of the Gyroscope 
     *
     * This function set the value of the gRes variable
     * This value is calculated as (gScale) / (2^15)
     * gScale must be set prior to calling this function
     */
    void calcgRes();

    /** Calculate the resolution of the Magnetometer 
     *
     * This function set the value of the mRes variable
     * This value is calculated as (mScale) / (2^15)
     * mScale must be set prior to calling this function
     */
    void calcmRes();

    /** Calculate the resolution of the Accelerometer 
     *
     * This function set the value of the aRes variable
     * This value is calculated as (aScale) / (2^15)
     * aScale must be set prior to calling this function
     */
    void calcaRes();
    
    /** This function give a default value if the scale was not defined by the user
     */
    void constrainScales();
    
    /** Initialize the I2C hardware
     *
     * This function will setup all I2C pins and related hardware
     */
    void initI2C();
    
    /** Write a byte out of I2C to a register in the device
     *
     *@param address       7-bit I2C address of the slave device
     *@param subAddress    Register to be written to
     *@param data          Byte to be written to the register           
     */
    void I2CwriteByte(uint8_t address, uint8_t subAddress, uint8_t data);

    /** Read a single byte from a register over I2C
     *
     *@param address       7-bit I2C address of the slave device
     *@param subAddress    Register to be read from
     *@return              The byte read from the requested address           
     */
    uint8_t I2CreadByte(uint8_t address, uint8_t subAddress);
    
     /** Read a series of bytes, starting at a register via I2C
     *
     *@param address       7-bit I2C address of the slave device
     *@param subAddress    Register to begin reading
     *@param[in]           dest is a pointer to an array to store the reading
     *@param count         The number of registers to be read
     *@return              No value is returned by the function but the registers read 
     *                     are stored in the *dest array              
     */
    uint8_t I2CreadBytes(uint8_t address, uint8_t subAddress, uint8_t * dest, uint8_t count);


    uint8_t _mAddress, _xgAddress; /* _mAddress and _xgAddress store the I2C address for each sensor */
    float gRes, aRes, mRes; /* gRes, aRes and mRes store the current resolution for each sensor */
    bool _autoCalc; /* keeps track of automatically substracting off bias calculated in calibration */
    
private:

    I2C i2c;  /* IO Device */
    int8_t status;  /* store the return value of I2C */

};

#endif /* LSM9DS1_H */
